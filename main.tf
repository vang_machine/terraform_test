resource "random_string" "random" {
  length           = 25
}

output "res" {
  value = random_string.random.result
}
